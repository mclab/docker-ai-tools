#!/bin/sh

xhost +local:docker

docker run \
	--mount type=bind,source="$(pwd)"/data,target=/data \
    -e DISPLAY=$DISPLAY \
    --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
    --net=host \
    mclab/ai_sapienza /opt/run_program.sh $@

xhost -local:docker

