# Artificial Intelligence course @ Computer Science, Sapienza

This Docker container gives you immediate availability of several AI tools, without the hassle of compilation and configuration.

# 1. Tools included in the container

The tools included in the container are listed below. To run a tool, use 

```./run.sh <command>```

where `<command>` is one of the commands below.

## SAT Solvers

A collection of several SAT solvers is available, developed in a time-span of 20+ years. 
Surprise yourself to see how much performance has improved in the years!

All such solvers take as input a CNF formula in DIMACS format, which can be easily generated using SATCodec.

### DPLL-based solvers

* Satz (1997). Command: `satz`
* eqzatz20 (1997). EqSatz is equivalence reasoning enhanced satz to solve satisfiability problems involving equivalence clauses.  Command: `eqsatz20`
* zChaff (2004) - Winner of the 2004 SAT Competition. [Web page](http://www.princeton.edu/~chaff/zchaff.html). Command: `zchaff`
* Minisat v1.14 (2005) - Evolution of the 2005 SAT Competition's runner up (v. 1.13). [Web page](http://minisat.se/MiniSat.html). Command: `minisat_v1.14`
* Minisat v2.2.0 (2007) - Third place at the 2007 SAT Competition. Command: `minisat_v2.2.0`
* minisatAllModels (~2008).  A (private) version of Minisat that enumerates all models of the input formula. This will be very useful to test the correctness of your encodings.  Command: `minisatAllModels`
* Picosat (2007) - Second place at the 2007 SAT Competition. [Web page](http://fmv.jku.at/picosat/).
* glucose (2009) [Web page](https://www.labri.fr/perso/lsimon/glucose/).  Command: `glucose`
* glucose-syrup 4.1 (2017) - Third place at the 2017 SAT+UNSAT Competition. [Web page](https://www.labri.fr/perso/lsimon/glucose/).  Command: `glucose-syrup`
* CaDiCaL (2017) - Winner at 2017 SAT+UNSAT / 2018 UNSAT Competition. [Web page](https://github.com/arminbiere/cadical).  Command: `cadical`


### Local-search solvers

* BG-WalkSat (2003) [Paper](https://www.cse.wustl.edu/~zhang/publications/bgwalksat-ijcai03.pdf) [Web page](https://www.swmath.org/software/5048).  Command: `bgwalksat`


## Genetic algorithms

CSPGeneticFramework is the result of a few exam projects carried out by Valentino Ascolese, Salvatore Maltese, Tommaso Pasini, Daniele Scarfini, Emanuele Vercalli, students of the AI course of the past years.
It provides a Java framework (based on abstract classes and interfaces with basic implementations) to assist you to build problem solvers 
that use genetic algorithms. 

The framework also provides a concrete implementation explicitly addressing the solving of CSPs. 
In particular, it allows you to model a new CSP problem via a text-based modelling language similar to Minizinc.
A GUI is offered as well, in which the user can monitor the population evolving during search.

Command: `CspGeneticFramework`



## Classical Planning

### LightHouse

LightHouse is the result of the exam project of Arteem Ageev, a student of the AI course from a past year.
This tool takes as input a textual representation of a planning domain (action schemas) and a planning problem (initial and goal states)
given in a simplified PDDL-like language (very similar to the clean FOL-based version of PDDL you have seen in the course) and:

* Translates the input planning domain and problem into actual PDDL (which can be a bit cumbersome to work with if you are not familiar with its syntax). By using Lighthouse in this mode, you can seamlessly run any available PDDL planner on your problems.
* Encodes the input planning problem into a SAT instance using the SATPlan approach. The tool relies on SATCodec and shows the translation into SAT step by step. This will be very useful to fully understand how the SATPlan encoding approach works. 

Command: `Lighthouse`

### Planners

A collection of recent PDDL planners can be downloaded from the website of the International Planning Competitions of the last years.


* 2018: https://ipc2018-classical.bitbucket.io/#planners
* 2014: https://helios.hud.ac.uk/scommv/IPC-14/



# 2. Installation

The following instructions will guide you in the creation of a docker container based on an image that contains several artificial intelligence tools. 
The user will be able to share files between the container and the host machine by using a shared folder that will be created during the installation.

## 2.1 Prerequisites
Before you get started, make sure to meet the following requirements:

1. You have a Unix or Windows machine.
2. Your user has ***root*** priviliges.
3. The port `8080` (Apache Webserver) is available. The ports exposed by the container to the host machine can be modified by editing the file `.env`.
4. You have [**docker**](https://www.docker.com/) and **git** installed.
    1. **Windows users**: it is advised to use [git for Windows OS](https://gitforwindows.org) and its integrated shell in order to follow this installation guide.
    2. **UNIX users**: it is advised to read the section 'Enable GUI-based software usage' before proceeding with the installation.
    
***Note***: The use of GUI-based tools, such as _CSPGeneticFramework_ and _LightHouse_, has only been tested on Ubuntu Linux. We would be glad to receive suggestions on how to support MacOS and Windows.

## 2.3 Installation and startup
1. Open a shell.
2. `cd` inside the directory where you want to install the container. From now on, we refer to such directory as `$BASE_DIR`.

3. Download this repository inside the current directory:
    1. click the "Clone" button in the upper-right corner of this page;
    2. choose the "https" method from the menu;
    3. copy the command you see (`git clone https://...`);
    4. paste the command in the shell.
 
4. Run `$BASE_DIR/build.sh`. This will download the latest version of the docker image.

# 3. Share files between container and host

The directory `data` in `$BASE_DIR$` is visible from inside the container, so it can be used to share files between your machine (the host) and the docker container.

# 4. Tests
To check that the installation completed correctely:

1. go to the URL <http://localhost:8080> in your web browser. If you modified the port in the `.env` file, use your port instead.
2. If you visualize the Apache Webserver landing page, your container was installed successfully!


You can test that every tool in Section 1 works by running on your host machine:

```./run.sh <tool> <arguments>```

You may need to add `sudo` before `./run.sh`, depending on your docker configuration.

For instance, you may test that `CSPGeneticFramework` works by running:

```./run.sh CspGeneticFramework /opt/demo/CSPGenetic/n-queens.txt```

(this should start the program's GUI on your machine),


# 5. Updates

To update the docker image, simply run ```$BASE_DIR/build.sh```.

***Note***: it is very likely that the content of this repository will be updated over time, so use ```git pull``` periodically to get the latest version of scripts, tool documentation and examples.


# People

## Contact
* **Marco Esposito** (<esposito@di.uniroma1.it>)


## Authors

* **Andrea Bacciu**  [Github profile](https://github.com/andreabac3)
* **Marco Esposito** (<esposito@di.uniroma1.it>)
* **Valerio Neri**   [Github profile](https://github.com/selektion)

