FROM mclab/ai_sapienza:latest

LABEL version=1.0
LABEL maintainer "Marco Esposito <esposito@di.uniroma1.it"
LABEL description="A Docker image with several AI tools"